

#import <UIKit/UIKit.h>

@interface PinchPanRotateVC : UIViewController
- (IBAction)handlePinchZoom:(id)sender;
- (IBAction)handlePan:(id)sender;
- (IBAction)handleRotate:(UIRotationGestureRecognizer *)recognizer;
@end
