

#import <UIKit/UIKit.h>

@interface RotateConcentricsVC : UIViewController
- (IBAction)handleRotate:(UIRotationGestureRecognizer *)recognizer;
- (IBAction)handleInnerRotate:(UIRotationGestureRecognizer *)recognizer;
- (IBAction)handleOuterPinch:(id)sender;



@end
